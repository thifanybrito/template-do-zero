function Comentario(){
  var nome = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var comentario = document.getElementById("areatexto").value;

  if (nome == "") {
    alert("Coloque seu nome!");
    return false;
  }
  if (email == "") {
    alert("Coloque seu e-mail");
    return false;
  }
  if (comentario == "") {
    alert("Faltou o comentário!");
    return false;
  }
  if (!/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/.test(nome)){
   alert("ATENÇÃO! Digite apenas letras no nome");
   return false;
  }
  if (!/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email)){
   alert('Digite um e-mail válido');
   return false;
  }
}
